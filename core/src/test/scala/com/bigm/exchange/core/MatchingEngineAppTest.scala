package com.bigm.exchange.core

import java.nio.file.Paths

import akka.stream.ActorMaterializer
import akka.testkit.TestFSMRef
import akka.util.Timeout
import com.bigm.exchange.client.{Balance, FileClientStorageService}
import com.bigm.exchange.order.OrderBook
import org.specs2.mutable.SpecificationWithJUnit

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}


class MatchingEngineAppTest extends SpecificationWithJUnit with Logging {

  "test actor matching engine" in new AkkaTestkitSpecs2Support {
    implicit val timeout = Timeout(10.seconds)
    implicit val materializer = ActorMaterializer()

    val clientFile = Paths.get(getClass.getResource("/clients.txt").toURI).toFile
    val clientStorageService = new FileClientStorageService(clientFile)
    clientStorageService.getBalance("C1") must beSome(Balance(1000,
      Map("A" -> 130, "B" -> 240, "C" -> 760, "D" -> 320)))

    val totalUSDSum = clientStorageService.getBalances().map(_._2).map(_.usd).sum
    val balances: Seq[Balance] = clientStorageService.getBalances().map(_._2)
    val orderTypes = balances.map(_.orderTypes)
    val totalOrderTypesSeq = OrderBook.Types.map { orderType =>
      orderTypes.flatMap(_.get(orderType)).sum
    }

    private val engineMap = OrderBook.Types.map { orderType =>
      (orderType, TestFSMRef(new MatchingEngineActor("test_actor", orderType, clientStorageService)))
    }.toMap

    val ordersPath = Paths.get(getClass.getResource("/orders.txt").toURI)
    val flow = MatchingEngineApp.flowOrders(ordersPath, engineMap)
    Await.result(flow, Duration.Inf)

    val newTotalUSDSum = clientStorageService.getBalances().map(_._2).map(_.usd).sum
     totalUSDSum mustEqual newTotalUSDSum

    val newBalances: Seq[Balance] = clientStorageService.getBalances().map(_._2)
    val newOrderTypes = newBalances.map(_.orderTypes)
    val newTotalOrderTypesSeq = OrderBook.Types.map { orderType =>
      newOrderTypes.flatMap(_.get(orderType)).sum
    }

    clientStorageService.getBalances().foreach { case (clientId, balance) =>
      log.info(s"$clientId $balance")
    }
     totalOrderTypesSeq mustEqual newTotalOrderTypesSeq
  }
}
