package com.bigm.exchange.client

import java.io.File

import scala.collection.mutable
import scala.io.Source

case class Balance(usd: Long, orderTypes: Map[String, Long])

sealed trait ClientStorageService {
  def updateBalance(clientId: String, balance: Balance): Balance

  def getBalance(clientId: String): Option[Balance]

  def getBalances(): Seq[(String, Balance)]
}

class FileClientStorageService(file: File) extends ClientStorageService {

  private object Locker

  private val clients: mutable.Map[String, Balance] = {
    val map = mutable.Map.empty[String, Balance]
    Source.fromFile(file)
      .getLines()
      .toSeq
      .foreach { line =>
        val Array(clientId, usd, orderA, orderB, orderC, orderD) = line.split("\t")
        val orderMap = Map[String, Long](
          "A" -> orderA.toLong,
          "B" -> orderB.toLong,
          "C" -> orderC.toLong,
          "D" -> orderD.toLong,
        )
        map.getOrElseUpdate(clientId, Balance(usd.toLong, orderMap))
      }
    map
  }

  override def updateBalance(clientId: String, balance: Balance): Balance = {
    Locker.synchronized {
      clients.update(clientId, balance)
      getBalance(clientId).get
    }
  }

  override def getBalance(clientId: String): Option[Balance] = {
    Locker.synchronized {
      clients.get(clientId)
    }
  }

  override def getBalances(): Seq[(String, Balance)] = {
    Locker.synchronized {
      clients.keys.flatMap { clientId =>
        clients.get(clientId).map((clientId, _))
      }.toSeq
    }
  }
}
