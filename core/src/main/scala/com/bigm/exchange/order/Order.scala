package com.bigm.exchange.order

object OrderBook {
  val Types = Seq("A", "B", "C", "D")
}

sealed trait Currency {
  def symbol: String

  def value: Long
}

case class USD(usdValue: Long) extends Currency {
  override def symbol = "$"

  override def value: Long = usdValue
}

sealed trait Order {
  def id: String

  def clientId: String

  def orderType: String

  def price: Currency

  def amount: Long
}

case class Bid(id: String, clientId: String, orderType: String, price: Currency, amount: Long) extends Order

object Bid {
  def apply(bid: Bid, newBidAmount: Long): Bid = {
    new Bid(bid.id, bid.clientId, bid.orderType, bid.price, newBidAmount)
  }
}

case class Ask(id: String, clientId: String, orderType: String, price: Currency, amount: Long) extends Order

object Ask {
  def apply(ask: Ask, newAskAmount: Long): Ask = {
    new Ask(ask.id, ask.clientId, ask.orderType, ask.price, newAskAmount)
  }
}
