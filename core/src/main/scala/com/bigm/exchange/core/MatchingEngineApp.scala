package com.bigm.exchange.core

import java.io.File
import java.nio.file.{Path, Paths}
import java.util.concurrent.atomic.AtomicLong

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{FileIO, Flow, Framing, Source}
import akka.util.{ByteString, Timeout}
import com.bigm.exchange.client.{Balance, FileClientStorageService}
import com.bigm.exchange.core.MatchingEngineActor.AddOrder
import com.bigm.exchange.order.{Ask, Bid, OrderBook, USD}
import com.typesafe.config.ConfigFactory

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration.{Duration, DurationInt}

object MatchingEngineApp {
  implicit val system: ActorSystem = ActorSystem()
  implicit val exct: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val timeout: Timeout = Timeout(10.seconds)
  private val config = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    val clientsFile = new File(config.getString("bigm.exchange.clients_path"))
    val clientStorageService = new FileClientStorageService(clientsFile)

    val engineMap = OrderBook.Types.map { orderType =>
      val props = MatchingEngineActor.props(s"engine_$orderType", orderType, clientStorageService)
      (orderType, system.actorOf(props))
    }.toMap

    val ordersFile = new File(config.getString("bigm.exchange.orders_path"))
    val flow = flowOrders(Paths.get(ordersFile.toURI), engineMap)
    Await.result(flow, Duration.Inf)
    Thread.sleep(5000)
    val resultsFile = new File(config.getString("bigm.exchange.results_path"))
    val csvFlow = Flow[(String, Balance)].map { case (clientId, balance) =>
      val params = Seq(
        clientId,
        balance.usd,
        balance.orderTypes.getOrElse("A", 0),
        balance.orderTypes.getOrElse("B", 0),
        balance.orderTypes.getOrElse("C", 0),
        balance.orderTypes.getOrElse("D", 0),
      )
      ByteString(params.mkString("\t") + "\n")
    }
    Source(clientStorageService.getBalances().toList)
      .via(csvFlow)
      .runWith(FileIO.toPath(Paths.get(resultsFile.toURI)))
      .onComplete(_ => system.terminate)
  }

  private[core] def flowOrders(ordersPath: Path, engineMap: Map[String, ActorRef]) = {
    val lineNumber: AtomicLong = new AtomicLong(0)
    FileIO.fromPath(ordersPath)
      .via(Framing.delimiter(ByteString("\n"), 256, allowTruncation = true).map(_.utf8String))
      .runForeach { line =>
        val Array(clientId, ticketType, orderType, price, amount) = line.split("\t")
        val index = lineNumber.incrementAndGet()
        ticketType match {
          case "b" =>
            val bid = new Bid(index.toString, clientId, orderType, USD(price.toLong), amount.toLong)
            engineMap(orderType) ! AddOrder(bid)
          case "s" =>
            val ask = new Ask(index.toString, clientId, orderType, USD(price.toLong), amount.toLong)
            engineMap(orderType) ! AddOrder(ask)
        }
      }
  }
}
