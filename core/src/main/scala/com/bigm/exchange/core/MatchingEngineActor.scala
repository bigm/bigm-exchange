package com.bigm.exchange.core

import akka.actor.{FSM, Props}
import com.bigm.exchange.client.{Balance, ClientStorageService}
import com.bigm.exchange.core.MatchingEngineActor._
import com.bigm.exchange.order.{Ask, Bid, Order}

import scala.concurrent.duration.{Duration, DurationInt}

object MatchingEngineActor {

  final case class AddOrder(order: Order)

  sealed trait State

  case object Idle extends State

  case object Active extends State

  sealed trait Data

  case object NoOrders extends Data

  case object AddOrders extends Data

  case object MatchOrders extends Data

  def props(id: String, orderType: String, clientStorageService: ClientStorageService): Props = {
    Props(classOf[MatchingEngineActor], id, orderType, clientStorageService: ClientStorageService)
  }
}

class MatchingEngineActor(id: String, orderType: String,
                          clientStorageService: ClientStorageService)
  extends FSM[MatchingEngineActor.State, MatchingEngineActor.Data] {
  private object Locker

  log.info("started")
  startWith(Idle, NoOrders)

  var bids: List[Bid] = Nil
  var asks: List[Ask] = Nil

  when(Idle) {
    case Event(AddOrder(order), NoOrders) =>
      self ! AddOrder(order)
      goto(Active) using MatchOrders
  }

  when(Active, stateTimeout = Duration.Zero) {
    case Event(StateTimeout, _) =>
      handleOrder()
    case Event(AddOrder(order), MatchOrders) =>
      addOrder(order)
  }

  private def addOrder(order: Order) = {
    log.info(s"Add order: $order")
    if (orderType != order.orderType) {
      log.error(s"Wrong ${order.orderType} $order")
      goto(Idle) using NoOrders
    } else {
      order match {
        case bid: Bid =>
          bids = (bid :: bids).sortWith(_.price.value > _.price.value)
        case ask: Ask =>
          asks = (ask :: asks).sortWith(_.price.value < _.price.value)
      }
      stay() using MatchOrders
    }
  }

  private def handleOrder() = {
    log.info(s"Start handle orders $orderType")
    if (bids.nonEmpty && asks.nonEmpty) {
      val bookHead = (bids.head, asks.head)
      bookHead match {
        case (bid, ask) if bid.price.value < ask.price.value =>
          log.info(s"No $orderType bids $bid for ask $ask")
          goto(Idle) using NoOrders
        case (bid, ask) if bid.price.value >= ask.price.value && bid.amount == ask.amount =>
          trade(bid, ask)
          bids = bids.tail
          asks = asks.tail
          stay() using MatchOrders
        case (bid, ask: Ask) if bid.price.value >= ask.price.value && bid.amount < ask.amount =>
          val matchingAsk = Ask(ask, bid.amount)
          val remainingAsk = Ask(ask, ask.amount - bid.amount)
          trade(bid, matchingAsk)
          bids = bids.tail
          asks = remainingAsk :: asks.tail
          stay() using MatchOrders
        case (bid, ask) if bid.price.value >= ask.price.value && bid.amount > ask.amount =>
          val matchingBid = Bid(bid, ask.amount)
          val remainingBid = Bid(bid, bid.amount - ask.amount)
          trade(matchingBid, ask)
          bids = remainingBid :: bids.tail
          asks = asks.tail
          stay() using MatchOrders
      }
    } else {
      log.info(s"Empty $orderType bids ${bids.length} or asks ${asks.length}")
      goto(Idle) using NoOrders
    }
  }

  def trade(bid: Bid, ask: Ask): Unit = {
    Locker.synchronized {
      for {
        askBalance <- clientStorageService.getBalance(ask.clientId)
        bidBalance <- clientStorageService.getBalance(bid.clientId)
      } yield {
        log.info(s"Trade $orderType $bid and $ask")
        val bidAmount = bidBalance.orderTypes(orderType)
        val askAmount = askBalance.orderTypes(orderType)
        val newBidAmount = bidAmount + ask.amount
        val newAskAmount = askAmount - ask.amount

        val newBidUSD = bidBalance.usd - ask.price.value
        val newAskUSD = askBalance.usd + ask.price.value

        val newBidBalanceTypes = (bidBalance.orderTypes - orderType) + (orderType -> newBidAmount)
        val newAskBalanceTypes = (askBalance.orderTypes - orderType) + (orderType -> newAskAmount)

        clientStorageService.updateBalance(bid.clientId, Balance(newBidUSD, newBidBalanceTypes))
        clientStorageService.updateBalance(ask.clientId, Balance(newAskUSD, newAskBalanceTypes))
      }
    }
  }

  initialize()
}
