# simple exchange

## run main class
```
mvn clean install
java -Dbigm.exchange.results_path=<path>/Matching/results.txt -Dbigm.exchange.clients_path=<path>/Matching/clients.txt -Dbigm.exchange.orders_path=<path>/Matching/orders.txt -cp ./core/target/core-0.0.1-SNAPSHOT.jar:./core/target/lib/* com.bigm.exchange.core.MatchingEngineApp
```
